<!-- ⚠️ This README has been generated from the file(s) "./.modules/docs/blueprint-contributing.md" ⚠️--><h1>Contributing Guide</h1>

First of all, thanks for visiting this page 😊 ❤️ ! We are totally ecstatic that you may be considering contributing to this project. You should read this guide if you are considering creating a pull request.


[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#table-of-contents)

## ➤ Table of Contents

* [➤ Code of Conduct](#-code-of-conduct)
* [➤ Overview](#-overview)
* [➤ Philosophy](#-philosophy)
* [➤ Requirements](#-requirements)
* [➤ Getting Started](#-getting-started)
	* [Descriptions of Build Scripts](#descriptions-of-build-scripts)
	* [Creating Dockerslim Builds](#creating-dockerslim-builds)
* [➤ Testing](#-testing)
	* [Testing Dockerslim Builds](#testing-dockerslim-builds)
	* [Testing Web Apps](#testing-web-apps)
* [➤ Linting](#-linting)
* [➤ Pull Requests](#-pull-requests)
	* [How to Commit Code](#how-to-commit-code)
	* [Pre-Commit Hook](#pre-commit-hook)

[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#code-of-conduct)

## ➤ Code of Conduct

This project and everyone participating in it is governed by the [Code of Conduct](https://gitlab.com/megabyte-labs/dockerfile/ansible-molecule/macos/-/blob/master/CODE_OF_CONDUCT.md). By participating, you are expected to uphold this code. Please report unacceptable behavior to [help@megabyte.space](mailto:help@megabyte.space).


[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#overview)

## ➤ Overview

All our Dockerfiles are created for specific tasks. In many cases, this allows us to reduce the size of the Dockerfiles by removing unnecessary files and performing other optimizations. [Our Dockerfiles](https://gitlab.com/megabyte-labs/dockerfile) are broken down into the following categories:

* **[Ansible Molecule](https://gitlab.com/megabyte-labs/dockerfile/ansible-molecule)** - Dockerfile projects used to generate pre-built Docker containers that are intended for use by Ansible Molecule
* **[Apps](https://gitlab.com/megabyte-labs/dockerfile/apps)** - Full-fledged web applications
* **[CI Pipeline](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline)** - Projects that include tools used during deployments such as linters and auto-formatters
* **[Software](https://gitlab.com/megabyte-labs/dockerfile/software)** - Docker containers that are meant to replace software that is traditionally installed directly on hosts


[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#philosophy)

## ➤ Philosophy

When you are working on one of our Dockerfile projects, try asking yourself, "How can this be improved?" By asking yourself that question, you might decide to take the project a step further by opening a merge request that:

* Reduces the size of the Docker container by converting it from a Ubuntu image to an Alpine image
* Improves the security and reduces the size of the Docker container by including a configuration for providing a Dockerslim build
* Linting the Dockerfile to conform with standards set in place by [Haskell Dockerfile Linter](https://github.com/hadolint/hadolint)

All of these improvements would be greatly appreciated by us and our community. After all, we want all of our Dockerfiles to be the best at what they do.


[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#requirements)

## ➤ Requirements

Before getting started with development, you should ensure that the following requirements are present on your system:

* **[Docker](repository.project.docker)**
* **[Dockerslim](https://gitlab.com/megabyte-labs/ansible-roles/dockerslim)** - Used for generating compact, secure images
* **[Node.js](https://gitlab.com/megabyte-labs/ansible-roles/nodejs)** (*Version >=10*) - Utilized to add development features like a pre-commit hook and other automations

*Each of the requirements links to an Ansible Role that can install the dependency with a one-line bash script install.*


[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#getting-started)

## ➤ Getting Started

To get started when developing one of our Dockerfile projects (after you have installed the requirements mentioned above), the first command you need to run in the root of the project is:

```
npm i
```

This will set up a pre-commit hook that will automatically lint your commits before committing. It will also refresh the repository to include the latest configurations. This ensures that other developers are using the same project code as you.

### Descriptions of Build Scripts

After you run `npm i`, you can view the various build commands by running `npm run info`. This will display a chart in your terminal with descriptions of the build scripts. It might look something like this:

```shell
❯ npm run info

> dockerfile-project@0.0.1 info
> npm-scripts-info

build:
  Build the Docker image on the local machine
build:slim:
  Build a slim Docker image with Dockerslim
commit:
  The preferred way of running git commit (instead of git commit, we prefer running 'npm run commit')
info:
  Logs descriptions of all the npm tasks
fix:
  Automatically fix formatting errors
prepare-release:
  Updates the CHANGELOG with commits made using 'npm run commit' and updates the project to be ready for release
shell:
  Run the Dockerfile and open its shell
test:
  Validates the Dockerfile and performs project linting
update:
  Runs .update.sh to automatically update meta files and documentation
version:
  Used by 'npm run prepare-release' to update the CHANGELOG and app version
```

You can then build the Docker image, for instance, by running `npm run build`. You can check out exactly what each command does by looking at the `package.json` file in the root of the project.

### Creating Dockerslim Builds

Whenever possible, a Dockerslim build should be provided and tagged as `:slim`. Dockerslim provides many configuration options so please check out the [Dockerslim documentation](https://github.com/docker-slim/docker-slim) to get a thorough understanding of it and what it is capable of. When you have formulated *and fully tested* the proper Dockerslim configuration, you can add it to the `package.json` next to the option that says `"build:slim"`.


[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#testing)

## ➤ Testing

Testing is an **extremely important** part of contributing to this project. Before opening a merge request, **you must test all common use cases of the Docker container**. This should be relatively straight-forward.

### Testing Dockerslim Builds

It is especially important to test Dockerslim builds. Dockerslim works by removing all the components in a container's operating system that it thinks are unnecessary. This can easily break things.

For example, if you are testing a Dockerslim build that packages [ansible-lint](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/ansible-lint) into a slim container, you might be tempted to simply test it by running `docker exec -it MySlimAnsibleLint ansible-lint`. This will ensure that the ansible-lint command can be accessed but that is not enough. You should also test it by passing in files as a volume and command line arguments. It is **important** to test all common use cases. Some people might be using the ansible-lint container in CI where the files are injected into the Docker container and some people might be using an inline command to directly access anible-lint from the host.

### Testing Web Apps

When testing Docker-based web applications, ensure that after you destroy the container you can bring the Docker container back up to its previous state using volumes and file mounts. This allows users to periodically update the Docker container while having their settings persist.


[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#linting)

## ➤ Linting

We utilize several different linters to ensure that all our Dockerfile projects use similar design patterns. Linting sometimes even helps spot errors as well. The most important linter for Dockerfile projects is called Haskell Dockerfile Linter (or hadolint). You can install it by utilizing our one-line installation method found in our [hadolint Ansible role](https://gitlab.com/megabyte-labs/ansible-roles/hadolint). In order for a merge request to be accepted, it has to successfully pass hadolint tests. For more information about hadolint, check out the [Haskell Dockerfile Linter GitHub page](https://github.com/hadolint/hadolint).

We also incorporate other linters that are run automatically whenever you commit code (assuming you have run `npm i` in the root of the project). These linters include:

* [Prettier](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/prettier)
* [Shellcheck](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/shellcheck)

Some of the linters are also baked into the CI pipeline. The pipeline will trigger whenever you post a commit to a branch. All of these pipeline tasks must pass in order for merge requests to be accepted. You can check the status of recently triggered pipelines for this project by going to the [CI/CD pipeline page](https://gitlab.com/megabyte-labs/dockerfile/ansible-molecule/macos/-/pipelines).


[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#pull-requests)

## ➤ Pull Requests

All pull requests should be associated with issues. You can find the [issues board on GitLab](https://gitlab.com/megabyte-labs/dockerfile/ansible-molecule/macos/-/issues). The pull requests should be made to [the GitLab repository](https://gitlab.com/megabyte-labs/dockerfile/ansible-molecule/macos) instead of the [GitHub repository](https://github.com/MegabyteLabs/docker-ansible-molecule-macos). This is because we use GitLab as our primary repository and mirror the changes to GitHub for the community.

### How to Commit Code

Instead of using `git commit`, we prefer that you use `npm run commit`. You will understand why when you try it but basically it streamlines the commit process and helps us generate better CHANGELOG files.

### Pre-Commit Hook

Even if you decide not to use `npm run commit`, you will see that `git commit` behaves differently because there is a pre-commit hook that installs automatically after you run `npm i`. This pre-commit hook is there to test your code before committing and help you become a better coder. If you need to bypass the pre-commit hook, then you may add the `--no-verify` tag at the end of your `git commit` command (e.g. `git commit -m "Commit" --no-verify`).

